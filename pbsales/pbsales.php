<?php


// ----0--9--8--7--6--5--4--3--2--1--1--2--3--4--5--6--7--8--9--0---- //
// ================================================================== //
//                                                                    //
//                  PBsales - Codecanyon Sales Alert                  //
//                                                                    //
// ================================================================== //
//                                                                    //
//                      Version 1.0 / 20.08.2015                      //
//                                                                    //
//                      Copyright 2015 - PB-Soft                      //
//                                                                    //
//                           www.pb-soft.com                          //
//                                                                    //
//                           Patrick Biegel                           //
//                                                                    //
// ================================================================== //


// =====================================================================
// Include the configuration.
// =====================================================================
include("config/config.php");


// #####################################################################
// #####################################################################
// ########   S E T   G E N E R A L   C U R L   O P T I O N S   ########
// #####################################################################
// #####################################################################


// =====================================================================
// Open a cURL channel.
// =====================================================================
$channel = curl_init();


// =====================================================================
// Set the cURL options - General information.
// =====================================================================
curl_setopt($channel, CURLOPT_HTTPHEADER, Array("Authorization: Bearer ".$api_token));
curl_setopt($channel, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($channel, CURLOPT_USERAGENT, 'API');



// #####################################################################
// #####################################################################
// #########   G E T   S A L E S   D A T A   F R O M   A P I   #########
// #####################################################################
// #####################################################################


// =====================================================================
// Set the cURL options - Sales information.
// =====================================================================
curl_setopt($channel, CURLOPT_URL, $api_sales_url);


// =====================================================================
// Execute the cURL request.
// =====================================================================
$json_sales_data = curl_exec($channel);


// =====================================================================
// Check if there was no error receiving the data - Sales info.
// =====================================================================
if ($json_sales_data !== false) {


  // ===================================================================
  // Get and decode the JSON data - Sales information.
  // ===================================================================
  $sales_data = json_decode($json_sales_data, true);


  // ===================================================================
  // Check if there was no error - Sales information.
  // ===================================================================
  if (json_last_error() == JSON_ERROR_NONE) {


    // #################################################################
    // #################################################################
    // #####   G E T   A C C O U N T   D A T A   F R O M   A P I   #####
    // #################################################################
    // #################################################################


    // =================================================================
    // Set the cURL options - Account information.
    // =================================================================
    curl_setopt($channel, CURLOPT_URL, $api_account_url);


    // =================================================================
    // Execute the cURL request.
    // =================================================================
    $json_account_data = curl_exec($channel);


    // =================================================================
    // Check if there was no error receiving the data - Account info.
    // =================================================================
    if ($json_account_data !== false) {


      // ===============================================================
      // Get and decode the JSON data - Account information.
      // ===============================================================
      $account_data = json_decode($json_account_data, true);


      // ===============================================================
      // Check if there was no error - Account information.
      // ===============================================================
      if (json_last_error() == JSON_ERROR_NONE) {


        // #############################################################
        // #############################################################
        // #######   P R O C E S S   R E T U R N E D   D A T A   #######
        // #############################################################
        // #############################################################


        // =============================================================
        // Get the sales data into variables.
        // =============================================================
        $item_price = sprintf("%01.2f", $sales_data[0]['item']['price_cents'] / 100);
        $item_sales_number = $sales_data[0]['item']['number_of_sales'];
        $item_gain = sprintf("%01.2f", $item_price * 0.5);
        $item_full_name = $sales_data[0]['item']['name'];
        $item_sold_at = $sales_data[0]['sold_at'];
        $item_sold_date = substr($item_sold_at, 8, 2).".".substr($item_sold_at, 5, 2).".".substr($item_sold_at, 0, 4);
        $item_sold_time = substr($item_sold_at, 11, 8);
        $item_timezone = substr($item_sold_at, 19, 6);
        $item_url = $sales_data[0]['item']['url'];


        // =============================================================
        // Get the account data into a variable.
        // =============================================================
        $account_balance = sprintf("%01.2f", $account_data['account']['balance']);


        // #############################################################
        // #############################################################
        // #########   G E T   I T E M   S H O R T   N A M E   #########
        // #############################################################
        // #############################################################


        // =============================================================
        // Search the item name for a delimiter string.
        // =============================================================
        $position = stripos($item_full_name, "-");


        // =============================================================
        // Check if the delimiter string was not found.
        // =============================================================
        if ($pos1 === false) {


          // ===========================================================
          // Specify the short name.
          // ===========================================================
          $item_short_name = $item_full_name;


          // ===========================================================
          // The delimiter string was found.
          // ===========================================================
        } else {


          // ===========================================================
          // Get the name parts from the name string.
          // ===========================================================
          $item_parts = explode("-", $item_full_name);


          // ===========================================================
          // Save the short name of the Codecanyon item.
          // ===========================================================
          $item_short_name = trim($item_parts[0]);

        } // The delimiter string was found.


        // =============================================================
        // Check if the account balance is greater or equal zero.
        // =============================================================
        if ($account_balance >= 0) {


          // ###########################################################
          // ###########################################################
          // ####   G E T   O L D   A C C O U N T   B A L A N C E   ####
          // ###########################################################
          // ###########################################################


          // ===========================================================
          // Check if the 'balance' file exist.
          // ===========================================================
          if (file_exists("balance")) {


            // =========================================================
            // Read the old balance and save the value into a variable.
            // =========================================================
            $old_balance = floatval(file_get_contents("balance"));


            // =========================================================
            // The 'balance' file does not exist.
            // =========================================================
          } else {


            // =========================================================
            // Set the old balance to zero.
            // =========================================================
            $old_balance = 0;

          } // The 'balance' file does not exist.


          // ===========================================================
          // Check if the old balance is greater or equal zero.
          // ===========================================================
          if ($old_balance >= 0) {


            // #########################################################
            // #########################################################
            // ##############   S E N D   E - M A I L S   ##############
            // #########################################################
            // #########################################################


            // =========================================================
            // Check if the new balance is higher than the old one (new
            // sale) or a test email should be sent to the receivers..
            // =========================================================
            if ($account_balance > $old_balance || (isset($_GET['email']) && intval($_GET['email']) == 1)) {


              // =======================================================
              // Check if the sending of e-mails is enabled.
              // =======================================================
              if ($send_email == 1) {


                // =====================================================
                // Specify the e-mail subject.
                // =====================================================
                $subject = "Codecanyon item sold: '".$item_short_name."' | Balance: ".$account_balance." USD";


                // =====================================================
                // Create the e-mail message.
                // =====================================================
                $message  = "\n";
                $message .= str_pad("", $line_total, "=")."\n";
                $message .= "Codecanyon item '".$item_full_name."' was sold!\n";
                $message .= str_pad("", $line_total, "=")."\n";
                $message .= "\n";
                $message .= "Email date: ".date('d.m.Y')."\n";
                $message .= "\n";
                $message .= "Email time: ".date('H:i:s')."\n";
                $message .= "\n";
                $message .= "Item short name: ".$item_short_name."\n";
                $message .= "\n";
                $message .= "Item full name: ".$item_full_name."\n";
                $message .= "\n";
                $message .= "Item URL: ".$item_url."\n";
                $message .= "\n";
                $message .= "Item sold date: ".$item_sold_date."\n";
                $message .= "\n";
                $message .= "Item sold time: ".$item_sold_time."\n";
                $message .= "\n";
                $message .= "Item timezone: ".$item_timezone."\n";
                $message .= "\n";
                $message .= "Item sales total: ".$item_sales_number."\n";
                $message .= "\n";
                $message .= "Item price: ".$item_price." USD\n";
                $message .= "\n";
                $message .= "Item gain: ".$item_gain." USD\n";
                $message .= "\n";
                $message .= "Account balance: ".$account_balance."\n";
                $message .= "\n";
                $message .= str_pad("", $line_total, "=")."\n";


                // =====================================================
                // Get the number of e-mail receivers.
                // =====================================================
                $receiver_number = count($email_receiver);


                // =====================================================
                // Loop through all the e-mail receivers.
                // =====================================================
                for ($receiver_counter = 0; $receiver_counter < $receiver_number; $receiver_counter++) {


                  // ===================================================
                  // Specify the e-mail header.
                  // ===================================================
                  $header  = "MIME-Version: 1.0\r\n";
                  $header .= "Content-Type: text/plain; charset=UTF-8\r\n";
                  $header .= "Content-Transfer-Encoding: 8bit\r\n";
                  $header .= "From: ".$email_sender."\r\n";
                  $header .= "Return-Path: ".$email_sender."\r\n\n";


                  // ===================================================
                  // Send the e-mail.
                  // ===================================================
                  @mail($email_receiver[$receiver_counter], $subject, $message, $header);

                } // Loop through all the e-mail receivers.

              } // Check if the sending of e-mails is enabled.

            } // Check if the new balance is higher than the old one (new sale) or a test email should be sent to the receivers.


            // #########################################################
            // #########################################################
            // #   W R I T E   N E W   A C C O U N T   B A L A N C E   #
            // #########################################################
            // #########################################################


            // =========================================================
            // Write the new account balance to the 'balance' file.
            // =========================================================
            file_put_contents("balance", $account_balance);


            // #########################################################
            // #########################################################
            // ############   D I S P L A Y   O U T P U T   ############
            // #########################################################
            // #########################################################


            // =========================================================
            // Check if the output parameter was specified.
            // =========================================================
            if (isset($_GET['output']) && intval($_GET['output']) == 1) {


              // =======================================================
              // Display the HTML header.
              // =======================================================
              echo "<!DOCTYPE html>\n";
              echo "<html lang=\"en\">\n";
              echo "<head>\n";
              echo "<meta charset=\"utf-8\">\n";
              echo "<title>Codecanyon Sales Alert</title>\n";
              echo "</head>\n";
              echo "<body>\n";


              // =======================================================
              // Display the result.
              // =======================================================
              echo "Item short name: ".htmlentities($item_short_name)."<br>\n";
              echo "Item full name: ".htmlentities($item_full_name)."<br>\n";
              echo "Item URL: <a href=\"".$item_url."\" onclick=\"window.open(this.href, 'Item page').focus(); return false;\">Item page</a><br>\n";
              echo "Item sold date: ".$item_sold_date."<br>\n";
              echo "Item sold time: ".$item_sold_time."<br>\n";
              echo "Item timezone: ".$item_timezone."<br>\n";
              echo "Item sales total: ".$item_sales_number."<br>\n";
              echo "Item price: ".$item_price." USD<br>\n";
              echo "Item gain: ".$item_gain." USD<br>\n";
              echo "Account balance: ".$account_balance."<br>\n";


              // =======================================================
              // Display the HTML footer.
              // =======================================================
              echo "</body>\n";
              echo "</html>\n";

            } else { echo "Error: 7"; } // Check if the output parameter was specified.

          } else { echo "Error: 6"; }  // Check if the old balance is greater or equal zero.

        } else { echo "Error: 5"; }  // Check if the account balance is greater or equal zero.

      } else { echo "Error: 4"; }  // Check if there was no error - Account information.

    } else { echo "Error: 3"; }  // Check if there was no error receiving the data - Account info.

  } else { echo "Error: 2"; }  // Check if there was no error - Sales information.

} else { echo "Error: 1"; }  // Check if there was no error receiving the data - Sales info.


// =====================================================================
// Close the cURL channel.
// =====================================================================
curl_close($channel);
