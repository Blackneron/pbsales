<?php


// ----0--9--8--7--6--5--4--3--2--1--1--2--3--4--5--6--7--8--9--0---- //
// ================================================================== //
//                                                                    //
//                  PBsales - Codecanyon Sales Alert                  //
//                                                                    //
// ================================================================== //
//                                                                    //
//                      Version 1.0 / 20.08.2015                      //
//                                                                    //
//                      Copyright 2015 - PB-Soft                      //
//                                                                    //
//                           www.pb-soft.com                          //
//                                                                    //
//                           Patrick Biegel                           //
//                                                                    //
// ================================================================== //


// #####################################################################
// #####################################################################
// ###################   C O N F I G U R A T I O N   ###################
// #####################################################################
// #####################################################################


// =====================================================================
// Please specify the default timezone.
// =====================================================================
//
// If the timezone is not defined in your PHP configuration file, you
// can specify it with the following setting:
//
//  date_default_timezone_set('UTC');
//
// You have to uncomment the setting and enter your preferred timezone.
// For a list of available timezones check the following link:
//
//   http://www.php.net/manual/en/timezones.php
//
// Normally it is easier to specify the default timezone in the PHP
// configuration file 'php.ini'. You have to add the following lines to
// the configuration file:
//
//   [Date]
//   date.timezone = UTC
//
// You then can replace 'UTC' with your own timezone.
//
// =====================================================================
date_default_timezone_set('Europe/Zurich');


// =====================================================================
// Specify the Envato/Codecanyon API url - Sales.
// =====================================================================
$api_sales_url = "https://api.envato.com/v2/market/author/sales?page=1";


// =====================================================================
// Specify the Envato/Codecanyon API url - Account.
// =====================================================================
$api_account_url = "https://api.envato.com/v1/market/private/user/account.json";


// =====================================================================
// Specify the Envato/Codecanyon API token (Bearer).
// =====================================================================
$api_token = "<--insert-your-API-token-here-->";


// =====================================================================
// Specify if sending emails is enabled.
// =====================================================================
$send_email = 1;


// =====================================================================
// Specify the email address of the sender.
// =====================================================================
$email_sender = "admin@example.com";


// =====================================================================
// Specify the email addresses of the receivers.
// =====================================================================
$email_receiver = array();
$email_receiver[] = "john@example.com";


// =====================================================================
// Specify the total line width for the emails (in characters).
// =====================================================================
$line_total = 80;
