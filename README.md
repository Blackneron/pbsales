# PBsales - README #
---

### Overview ###

The **PBsales** tool can contact the Envato/Codecanyon API and check periodically (cronjob) if an item from your Codecanyon portfolio was sold and then sends you an e-mail with information about the transaction.

### Screenshots ###

![PBsales E-Mail Alert](development/readme/pbsales01.png "PBsales E-Mail Alert")

![PBsales HTML Output](development/readme/pbsales02.png "PBsales HTML Output")

### Setup ###

* Upload the **PBsales** tool directory (**pbsales**) to your webhost.
* Make sure that the script can write to his own directory.
* Edit the configuration file **config/config.php** and enter the API token and e-mail addresses etc.
* Add a cronjob which periodically runs the script **pbsales.php**.
* If you run the script in the webbrowser the parameter **output=1** displays infos.
* If you run the script in the webbrowser the parameter **email=1** sends a test e-mail.

### Support ###

This is a free script and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBsales** tool is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
